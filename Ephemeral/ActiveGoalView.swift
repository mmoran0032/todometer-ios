//
//  SwiftUIView.swift
//  todometer
//
//  Created by Mike Moran on 5/8/22.
//

import SwiftUI

struct ActiveGoalView: View {
    @Binding var goals: [Goal]
    @Binding var isEditing: Bool
    
    @State private var currentName: String = ""
    
    var body: some View {
        VStack {
            if (goals.count == 0) || goals[0].name.isEmpty  {
                
                HStack {
                    TextField("I want to accomplish...", text: $currentName)
                        .font(.headline)
                    
                    Button(action: {
                        withAnimation {
                            if goals.count == 0 { goals.append(Goal(name: currentName)) }
                            goals[0].name = currentName
                            currentName = ""
                        }
                    }) {
                        Image(systemName: "checkmark.circle.fill")
                            .accessibilityLabel("Update current goal")
                    }
                    .disabled(currentName.isEmpty)
                }
                .padding()
                
            } else {
                
                Text(goals[0].name)
                    .font(.headline)
                    .padding()
                
                GoalProgressView(goal: $goals[0], data: goals[0].data)
                
                HStack {
                    Text("\(Image(systemName: "calendar"))")
                    Text(goals[0].startDate, style: .date)
                    
                    Spacer()
                    
                    Label("\(Int(goals[0].totalDays)) days", systemImage: "hourglass.bottomhalf.fill")
                    
                    Spacer()
                    
                    Button(action: {
                        isEditing = true
                    }) {
                        Image(systemName: "pencil.circle")
                            .accessibilityLabel("Update current goal")
                    }
                    .font(.body)
                }
                .font(.caption)
                .padding()
                
            }
        }
    }
}

struct ActiveGoalView_Previews: PreviewProvider {
    static var previews: some View {
        ActiveGoalView(goals: .constant([Goal.sampleGoal, Goal.sampleGoal]), isEditing: .constant(false))
            .previewLayout(.sizeThatFits)
            .padding(32)
        
        ActiveGoalView(goals: .constant([Goal.sampleGoal]), isEditing: .constant(false))
            .previewLayout(.sizeThatFits)
            .padding(32)
        
        ActiveGoalView(goals: .constant([Goal(name: ""), Goal.sampleGoal]), isEditing: .constant(false))
            .previewLayout(.sizeThatFits)
            .padding(32)
        
        ActiveGoalView(goals: .constant([Goal(name: "")]), isEditing: .constant(false))
            .previewLayout(.sizeThatFits)
            .padding(32)
        
        ActiveGoalView(goals: .constant([]), isEditing: .constant(false))
            .previewLayout(.sizeThatFits)
            .padding(32)
    }
}
