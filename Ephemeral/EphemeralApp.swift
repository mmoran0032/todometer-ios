//
//  todometerApp.swift
//  todometer
//
//  Created by Mike Moran on 5/7/22.
//

import SwiftUI

@main
struct EphemeralApp: App {
    @StateObject private var store = GoalStore()
    
    var body: some Scene {
        WindowGroup {
            NavigationView {
                GoalOverview(
                    goals: $store.goals,
                    data: store.goals[0].data
                ) {
                    GoalStore.save(goals: store.goals) { result in
                        if case .failure(let error) = result {
                            fatalError(error.localizedDescription)
                        }
                    }
                }
            }
            .onAppear {
                GoalStore.load() { result in
                    switch result {
                    case .failure(let error): fatalError(error.localizedDescription)
                    case .success(let goals): store.goals = goals
                    }
                }
            }
        }
    }
}
