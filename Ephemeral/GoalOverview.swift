//
//  GoalView.swift
//  todometer
//
//  Created by Mike Moran on 5/7/22.
//

import SwiftUI

struct GoalOverview: View {
    @Binding var goals: [Goal]

    @State var isEditing: Bool = false
    
    @Environment(\.scenePhase) private var scenePhase
    
    @State var data: Goal.Data = Goal.Data()

    let saveAction: ()->Void

    var body: some View {
        
        GoalsListView(goals: $goals, isEditing: $isEditing)
        .padding(32)
        .navigationTitle("Ephemeral")
        .toolbar {
            ToolbarItem(placement: .confirmationAction) {
                Button(action: {
                    goals = [Goal(name: "")]
                }) {
                    Image(systemName: "exclamationmark.arrow.circlepath")
                }
                .accessibilityLabel("Remove all progress")
                .disabled(goals.count <= 1)
            }
            
            ToolbarItemGroup(placement: .bottomBar) {
            
                Spacer()
                
                Button(action: {
                    data = goals[0].data
                    data.reject += 1
                    goals[0].update(from: data)
                }) {
                    Image(systemName: "arrowtriangle.left")
                        .font(.headline)
                }
                .accessibilityLabel("Remove progress")
                .disabled(goals.isEmpty || goals[0].name.isEmpty)
                .padding()
            
                Spacer()
                
                Button(action: {
                    // freeze the goal
                    data = goals[0].data
                    data.endDate = Date.now
                    goals[0].update(from: data)
                    
                    goals.insert(Goal(name: ""), at: 0)
                    data = goals[0].data
                    
                    // show up to 5 goals (5 archived, 1 active 4 archived)
                    while goals.count > 5 {
                        goals.removeLast()
                    }
                }) {
//                    Image(systemName: "exclamationmark.arrow.circlepath")
                    Image(systemName: "square.and.arrow.down")
                        .font(.headline)
                }
                .accessibilityLabel("Archive current goal")
                .disabled(goals.isEmpty || goals[0].name.isEmpty)
                .padding()
                
                Spacer()
                
                Button(action: {
                    data = goals[0].data
                    data.affirm += 1
                    goals[0].update(from: data)
                }) {
                    Image(systemName: "arrowtriangle.right")
                        .font(.headline)
                }
                .accessibilityLabel("Make progress")
                .disabled(goals[0].name.isEmpty)
                .padding()
                
                Spacer()
            }
        }
        .onChange(of: scenePhase) { phase in
            if phase == .inactive { saveAction() }
        }
        .sheet(isPresented: $isEditing) {
            GoalEditView(goalName: $goals[0].name, goalStart: $goals[0].startDate, isEditing: $isEditing)
        }
    }
}

struct GoalOverview_Previews: PreviewProvider {
    static var previews: some View {
        GoalOverview(
            goals: .constant([Goal.sampleGoal, Goal.sampleGoal]),
            data: Goal.sampleGoal.data,
            saveAction: {}
        )
        
        GoalOverview(
            goals: .constant([Goal(name: ""), Goal.sampleGoal]),
            data: Goal.Data(),
            saveAction: {}
        )
        
        GoalOverview(
            goals: .constant([Goal(name: ""), Goal.sampleGoal, Goal.sampleGoal]),
            data: Goal.Data(),
            saveAction: {}
        )
    }
}
