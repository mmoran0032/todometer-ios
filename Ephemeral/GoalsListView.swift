//
//  ArchivedListView.swift
//  Todometer
//
//  Created by Mike Moran on 5/8/22.
//

import SwiftUI

struct GoalsListView: View {
    @Binding var goals: [Goal]
    @Binding var isEditing: Bool
    
    var body: some View {
        ZStack {
            VStack {
                ActiveGoalView(goals: $goals, isEditing: $isEditing)
                    .padding()
                
                Spacer()
            }
            
            VStack {
                Spacer()
                    .ignoresSafeArea(.keyboard)
                    
                if goals.count > 1 {
                    
                    Text("Previous goals")
                        .font(.headline)
                        .padding()
                    
                    // we'll want to build up a list ourselves...
                    ForEach($goals.suffix(from: 1)) { goal in
                        ArchivedGoalView(archivedGoal: goal)
                            .padding(.bottom)
                    }
                }
            }
            .ignoresSafeArea(.keyboard)
        }
    }
}

struct ArchivedListView_Previews: PreviewProvider {
    static var previews: some View {
        GoalsListView(
            goals: .constant([Goal.sampleGoal, Goal.sampleGoal, Goal.sampleGoal]),
            isEditing: .constant(false)
        )
            .previewLayout(.sizeThatFits)
            .padding(32)
        
        GoalsListView(goals: .constant([Goal.sampleGoal]), isEditing: .constant(false))
            .previewLayout(.sizeThatFits)
            .padding(32)
        
        GoalsListView(goals: .constant([Goal(name: "")]), isEditing: .constant(false))
            .previewLayout(.sizeThatFits)
            .padding(32)
        
        GoalsListView(goals: .constant([Goal(name: ""), Goal.sampleGoal]), isEditing: .constant(false))
            .previewLayout(.sizeThatFits)
            .padding(32)
    }
}
