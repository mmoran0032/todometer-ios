// Create simple bar chart showing progress

import SwiftUI

struct GoalProgressView: View {
    @Binding var goal: Goal
    
    @State var data: Goal.Data
    
    var body: some View {        
        HStack {
            ProgressView(value: goal.progress.pctReject)
                .tint(Color("eph-brown"))
                .scaleEffect(x: -1, y: 4, anchor: .center)
            
            ProgressView(value: goal.progress.pctAffirm)
                .tint(Color("eph-green"))
                .scaleEffect(x: 1, y: 4, anchor: .center)
        }
    }
}

struct GoalProgressView_Previews: PreviewProvider {
    static var previews: some View {
        GoalProgressView(goal: .constant(Goal.sampleGoal), data: Goal.sampleGoal.data)
            .previewLayout(.sizeThatFits)
            .preferredColorScheme(.dark)
            .padding(32)
    }
}
